#! /usr/bin/env python

import cv2
import rospy
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from std_msgs.msg import Int64


class PixelCounter:

    def __init__(self):
        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("image", Image, self.callback)
        self.counter_pub = rospy.Publisher("pixel_count", Int64)

    def callback(self, data):
        # convert to grayscale opencv image
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "mono8")
        except CvBridgeError as e:
            print(e)
        msg = Int64()
        msg.data = cv2.countNonZero(cv_image)
        self.counter_pub.publish(msg)


def main():
    rospy.init_node('count_pixels', anonymous=True)
    pixel_counter = PixelCounter()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
