# About
ROS node that counts the number of pixels of a given color in an image.


An example launch file is provided, which uses the [opencv_apps](http://wiki.ros.org/opencv_apps) image_filter nodes to segment the images and count the pixels using `cv2.countNonZero`.

# pixel_counter.py
The core of this package is the pixel_counter.py node.
This node subscribes to an image topic, counts all non-black pixels and publishes the result

## Subsciptions
* `/image` : sensor_msgs.Image - Pixels are counted for these received images.

## Publications
* `/pixel_count`: std_msgs.Int64 - Number of non-black pixels in the image.